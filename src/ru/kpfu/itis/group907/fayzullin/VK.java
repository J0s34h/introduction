package ru.kpfu.itis.group907.fayzullin;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashSet;

public class VK {
    // Any API Token
    private static final String TOKEN = "&access_token=INSERT_YOUR_TOKEN";

    private static final String BASE_URL = "https://api.vk.com/method/";
    private static final String API_VERSION = "?v=5.20&";

    private static class BadResponse extends Exception {
        // Marker exception to indicate that server response resulted in "error"
    }

    private static JsonObject makeInquiry(String method, String data) throws BadResponse {
        // Universal Method for all inquiries to the API server
        InputStream stream = null;

        try {
            URL url = new URL(BASE_URL + method + API_VERSION + data + TOKEN);
            URLConnection connection = url.openConnection();
            stream = connection.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }

        StringBuilder response = new StringBuilder();

        // In case connection doesn't make it
        assert stream != null;

        int c;
        try {
            while (((c = stream.read()) != -1)) {
                response.append((char) c);
            }
        } catch (IOException e) {
            // Corrupted response
            e.printStackTrace();
        }

        JsonObject result = ((JsonObject) JsonParser.parseString(response.toString()));

        if (result.has("response")) {
            return result.get("response").getAsJsonObject();
        } else {
            // Doesn't contain response field -> bad
            throw new BadResponse();
        }
    }

    public static JsonObject getUser(String userId) throws BadResponse {
        return makeInquiry("users.get", "user_ids=" + userId);
    }

    public static JsonObject getFriends(String userId) throws BadResponse {
        return makeInquiry("friends.get", "user_id=" + userId);
    }

    public static JsonObject getSubscriptions(String userId) throws BadResponse {
        return makeInquiry("users.getSubscriptions", "user_id=" + userId);
    }

    public static ArrayList<String> getFriendsWithSimilarSubscriptions(String userId) {
        HashSet<String> subscriptions = new HashSet<>();
        ArrayList<String> friends = new ArrayList<>();

        JsonObject response;

        try {
            response = getSubscriptions(userId);
            JsonArray targetSubscriptions = response.get("groups").getAsJsonObject().get("items").getAsJsonArray();

            targetSubscriptions.forEach((o) -> subscriptions.add(o.toString()));

            response = getFriends(userId);

            response.get("items").getAsJsonArray().forEach((friend) -> {
                try {
                    JsonArray array;
                    array = getSubscriptions(friend.toString()).get("groups").getAsJsonObject().get("items").getAsJsonArray();

                    Gson parser = new Gson();

                    // Converting to String array as JsonArray from google doesn't support stream,
                    // and here we only need at least one matching subscription

                    String[] friendsSubscriptions = parser.fromJson(array, String[].class);

                    for (String friendsSubscription : friendsSubscriptions) {
                        if (subscriptions.contains(friendsSubscription)) {
                            friends.add(friend.toString());
                            break;
                        }
                    }

                } catch (BadResponse badResponse) {
                    // Some friend locked his profile
                }
            });

        } catch (BadResponse badResponse) {
            // Target User has locked his profile, there is nothing to be done about it...
            badResponse.printStackTrace();
        }

        return friends;
    }

    public static void main(String[] args) {
        // (My id in VK) -> for me it is 15
        System.out.println(getFriendsWithSimilarSubscriptions("557923203").size());
    }
}
