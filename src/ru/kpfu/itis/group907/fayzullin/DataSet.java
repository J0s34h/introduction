package ru.kpfu.itis.group907.fayzullin;

import com.google.gson.JsonArray;

import java.io.*;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class DataSet {
    public static void main(String[] args) {
        try {
            getCharts();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void getCharts() throws IOException {
        BufferedReader reader = null;
        ArrayList<String> product = new ArrayList<>();

        try {
            reader = new BufferedReader(new FileReader("transactions.csv"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        assert reader != null;

        // Read header
        String row = reader.readLine();

        while ((row = reader.readLine()) != null) {
            product.add(row.split(";")[0]);
        }

        Collections.sort(product);

        HashMap<String, Integer> productSales = new HashMap<>();
        String productId = product.get(0);
        int productEncounters = 1;

        // Create hash map where key is product, and values in encounter count

        for (int i = 1; i < product.size(); i++) {
            if (product.get(i).equals(productId)) {
                productEncounters++;
            } else {
                productSales.put(productId, productEncounters);

                productId = product.get(i);
                productEncounters = 1;
            }
        }

        // Sort hash map by frequency from lower to higher

        productSales = sortByFrequency(productSales);

        // Save results in .csv

        PrintWriter writer = null;

        try {
            writer = new PrintWriter(new BufferedWriter(new FileWriter("frequency.csv")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        assert writer != null;

        StringBuilder output = new StringBuilder();

        productSales.forEach((k, v) -> {
            output.append(k + "," + v).append("\n");
        });

        writer.write(output.toString());

        writer.close();
    }

    private static HashMap<String, Integer> sortByFrequency(HashMap<String, Integer> map) {
        List<Map.Entry<String, Integer>> list = new LinkedList<>(map.entrySet());

        list.sort((Comparator) (o1, o2) -> {
            return ((Comparable) ((Map.Entry) (o1)).getValue()).compareTo(((Map.Entry) (o2)).getValue());
        });

        //LinkedHashMap has predictable iteration order

        HashMap<String, Integer> result = new LinkedHashMap<>();

        for (Map.Entry<String, Integer> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }

}
